import React, { useState } from 'react';
import { User } from 'pages/Chat/components/User';
import { chatRequest } from 'api/rest/chat';

export const Users = () => {
    const [usersData, setUsersData] = useState(chatRequest.getUsers());

    if (usersData.length === 0) {
        return <div>Пользователей нет</div>;
    }

    return (
        <React.Fragment>
            {usersData?.map((user) => (
                <User key={user?.id} name={user?.name} to={user?.id} />
            ))}
        </React.Fragment>
    );
};

import React from 'react';
import { Users } from './components/Users';
import styles from './ChatLayout.module.scss';

interface IChatLayout {
    children: React.ReactNode;
}

export const ChatLayout = ({ children }: IChatLayout) => {
    return (
        <>
            <main>
                <section className={styles.Wrapper}>
                    <div className={styles.Users}>
                        <Users />
                    </div>
                    <div className={styles.Dialog}>{children}</div>
                </section>
            </main>
        </>
    );
};

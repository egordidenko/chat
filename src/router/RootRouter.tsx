import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ChatRouter } from './Chat/ChatRouter';
import { chatRoutes } from './Chat/chatRoutes';

export const RootRouter: React.FC = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path={chatRoutes.BASE}>
                    <ChatRouter />
                </Route>
            </Switch>
        </BrowserRouter>
    );
};

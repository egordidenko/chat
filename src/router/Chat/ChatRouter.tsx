import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Chat } from 'pages/Chat';
import { ChatLayout } from 'layouts/ChatLayout/ChatLayout';

import { chatRoutes } from './chatRoutes';

export const ChatRouter: React.FC = () => {
    return (
        <ChatLayout>
            <Switch>
                <Route path={chatRoutes.getChatId(':chatId')} exact={true}>
                    <Chat />
                </Route>
            </Switch>
        </ChatLayout>
    );
};

export const chatRoutes = {
    BASE: '/chat',
    getChatId: (chatId: string | number) => `/chat/${chatId}`,
};

import React from 'react';

import { Messages } from './components/Messages';

export const Chat = () => {
    return (
        <React.Fragment>
            <Messages />
        </React.Fragment>
    );
};

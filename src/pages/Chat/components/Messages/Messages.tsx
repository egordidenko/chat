import React, { useEffect, useRef, useState } from 'react';

import styles from './Messages.module.scss';
import { useParams } from 'react-router-dom';

export const Messages = () => {
    const { chatId }: any = useParams();

    const localMessages = localStorage.getItem('messages');
    const parseMessages = JSON.parse(localMessages as string);

    const [messagesData, setMessagesData] = useState<any>(parseMessages || []);

    useEffect(() => {
        localStorage.setItem('messages', JSON.stringify(messagesData));
    }, [messagesData]);

    useEffect(() => {
        inputRef.current.value = '';
    }, [chatId]);

    const inputRef = useRef<any>();

    const handleSendMessage = async (event: any) => {
        event.preventDefault();

        const value = inputRef.current.value;
        let lastMessageId = messagesData[messagesData.length - 1]?.id || 1;
        const incrementId = ++lastMessageId;

        await setMessagesData((prevState: any) => [
            ...prevState,
            {
                id: incrementId,
                text: value,
                user_id_from: chatId === '1' ? 1 : 2,
                user_id_to: chatId === '1' ? 2 : 1,
                type: chatId === '1' ? `from` : 'me',
            },
        ]);

        inputRef.current.value = '';
    };
    return (
        <div>
            <ul className={styles.Messages}>
                {messagesData?.map((message: any) => (
                    <li
                        style={{
                            marginLeft: message?.type === 'from' ? 'auto' : '',
                        }}
                        key={message.id}
                    >
                        {message.text}
                    </li>
                ))}
            </ul>

            <form method="POST" onSubmit={handleSendMessage}>
                <input
                    ref={inputRef}
                    type="text"
                    placeholder="Введите ваше сообщение"
                />
                <button>отправить</button>
            </form>
        </div>
    );
};

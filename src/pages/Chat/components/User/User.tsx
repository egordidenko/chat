import React from 'react';
import { Link } from 'react-router-dom';
import styles from './User.module.scss';
import { chatRoutes } from 'router/Chat/chatRoutes';

interface IUser {
    name?: string;
    to?: any;
}

export const User = ({ name, to }: IUser) => {
    return (
        <div className={styles.UserWrapper}>
            <Link to={chatRoutes.getChatId(to)}>
                <h1>{name}</h1>
            </Link>
        </div>
    );
};

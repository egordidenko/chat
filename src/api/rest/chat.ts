const schemaUser = [
    {
        id: 1,
        name: 'Пользователь',
        surname: 'Иван',
        photo: '',
        href: '/chat/1',
    },
    {
        id: 2,
        name: 'Друг',
        surname: 'Иван',
        photo: '',
        href: '/chat/2',
    },
];

const schemaChat = [
    {
        chat_id: 1,
        messages: [
            {
                message_id: 1,
                text: '',
                user_id_from: '',
                user_id_to: '',
                type: '',
            },
        ],
    },
];

const getMessages = () => {
    return schemaChat;
};

const getUsers = () => {
    return schemaUser;
};

export const chatRequest = { getMessages, getUsers };

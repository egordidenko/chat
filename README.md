# Basic structure

```
/root 
  \__ /src
      \__ /components
      \__ /contexts
      \__ /hooks
      \__ /layouts
      \__ /lib
      \__ /middleware
      \__ /modules
      \__ /pages
      \__ /providers
      \__ /rest
      \__ /router
      \__ /services
      \__ /utils
```
